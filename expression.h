// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#ifndef REGJS_EXPRESSION_H
#define REGJS_EXPRESSION_H

#include "reglibcpp/expression.h"
#include "nfa.h"
#include "utils.h"

namespace reg {
struct Expression {
  expression::exptr wrapee;
  inline Expression() {}
  inline Expression(expression::exptr&& e) : wrapee(std::move(e)) {}
  inline Expression(const expression::exptr& e) : wrapee(e) {}

  inline std::vector<Expression> children() const {
    return std::vector<Expression>(wrapee->begin(), wrapee->end());
  }

  inline std::wstring toString() const {
    return u8tojs(wrapee->to_string());
  }

  inline std::wstring extractSymbol() const {
    return u8tojs(wrapee->extractSymbol());
  }

  inline expression::operation getOperation() const {
    return wrapee->getOperation();
  }

  inline size_t size() const {
    return wrapee->size();
  }

  inline bool equals(Expression other) const {
    return *wrapee == *other.wrapee;
  }

  inline bool equalsNFA(NFA other) const {
    return *wrapee == other;
  }

  inline NFA asNFA() const {
    return static_cast<nfa>(*wrapee);
  }
};

inline Expression REEmptySet() {
  return expression::spawnEmptySet();
}

inline Expression REEmptyString() {
  return expression::spawnEmptyString();
}

inline Expression RESymbol(std::wstring symbol) {
  return expression::spawnSymbol(jstou8(symbol));
}

inline Expression REAlternation(Expression l, Expression r) {
  return expression::spawnAlternation(l.wrapee, r.wrapee);
}

inline Expression REAlternationO0(Expression l, Expression r) {
  return expression::spawnAlternation(l.wrapee, r.wrapee, false, false);
}

inline Expression REAlternationO1(Expression l, Expression r) {
  return expression::spawnAlternation(l.wrapee, r.wrapee, true, false);
}

inline Expression REAlternationO2(Expression l, Expression r) {
  return expression::spawnAlternation(l.wrapee, r.wrapee, true, true);
}

inline Expression REConcatenation(Expression l, Expression r) {
  return expression::spawnConcatenation(l.wrapee, r.wrapee);
}

inline Expression REConcatenationO0(Expression l, Expression r) {
  return expression::spawnConcatenation(l.wrapee, r.wrapee, false, false);
}

inline Expression REConcatenationO1(Expression l, Expression r) {
  return expression::spawnConcatenation(l.wrapee, r.wrapee, true, false);
}

inline Expression REConcatenationO2(Expression l, Expression r) {
  return expression::spawnConcatenation(l.wrapee, r.wrapee, true, true);
}

inline Expression REKleene(Expression b) {
  return expression::spawnKleene(b.wrapee);
}

inline Expression REKleeneO0(Expression b) {
  return expression::spawnKleene(b.wrapee, false, false);
}

inline Expression REKleeneO1(Expression b) {
  return expression::spawnKleene(b.wrapee, true, false);
}

inline Expression REKleeneO2(Expression b) {
  return expression::spawnKleene(b.wrapee, true, true);
}

inline Expression REFromString(std::wstring re) {
  return expression::spawnFromString(jstou8(re));
}

inline Expression REFromStringO0(std::wstring re) {
  return expression::spawnFromString(jstou8(re), false, false);
}

inline Expression REFromStringO1(std::wstring re) {
  return expression::spawnFromString(jstou8(re), true, false);
}

inline Expression REFromStringO2(std::wstring re) {
  return expression::spawnFromString(jstou8(re), true, true);
}

inline void RESetLeftParenthesis(std::wstring L) {
  expression::L = converter.from_bytes(jstou8(L))[0];
}

inline std::wstring REGetLeftParenthesis() {
  return u8tojs(converter.to_bytes(expression::L));
}

inline void RESetRightParenthesis(std::wstring R) {
  expression::R = converter.from_bytes(jstou8(R))[0];
}

inline std::wstring REGetRightParenthesis() {
  return u8tojs(converter.to_bytes(expression::R));
}

inline void RESetKleeneStar(std::wstring K) {
  expression::K = converter.from_bytes(jstou8(K))[0];
}

inline std::wstring REGetKleeneStar() {
  return u8tojs(converter.to_bytes(expression::K));
}

inline void RESetAlternationSign(std::wstring A) {
  expression::A = converter.from_bytes(jstou8(A))[0];
}

inline std::wstring REGetAlternationSign() {
  return u8tojs(converter.to_bytes(expression::A));
}

inline void RESetEmptyString(std::wstring E) {
  expression::E = converter.from_bytes(jstou8(E))[0];
}

inline std::wstring REGetEmptyString() {
  return u8tojs(converter.to_bytes(expression::E));
}

inline void RESetEmptySet(std::wstring N) {
  expression::N = converter.from_bytes(jstou8(N))[0];
}

inline std::wstring REGetEmptySet() {
  return u8tojs(converter.to_bytes(expression::N));
}

}

#endif
