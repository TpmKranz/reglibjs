// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#include "reglibcpp/fabuilder.h"
#include "nfa.h"
#include "dfa.h"

namespace reg {
struct FABuilder : public fabuilder {
  FABuilder();
  FABuilder(NFA n);
  FABuilder(nfa n);
  FABuilder(dfa d);
  FABuilder(fabuilder b);

  void setAccepting(std::wstring state, bool accept);
  void makeInitial(std::wstring state);
  void addSymbol(std::wstring symbol);
  void addTransition(std::wstring from, std::wstring to, std::wstring symbol);
  void powerset();
  void complete();
  void merge();
  void purge();
  void minimize();
  void unite(NFA other);
  void intersect(NFA other);
  void subtract(NFA other);
  void complement();
  void normalizeStateNames(std::wstring prefix);
  bool isComplete();
  bool hasNondeterminism();
  NFA buildNFA();
  DFA buildDFA();
};
}
