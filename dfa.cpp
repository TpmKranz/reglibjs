// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#include "dfa.h"
#include "utils.h"
#include <emscripten/bind.h>

namespace reg {
DFA::DFA() : dfa() {}
DFA::DFA(dfa d) : dfa(std::move(d)) {}

std::wstring DFA::delta(std::wstring q, std::wstring symbol) const {
  return u8tojs(dfa::delta(jstou8(q), jstou8(symbol)));
}

std::wstring DFA::deltaHat(std::wstring q, std::wstring word) const {
  return u8tojs(dfa::deltaHat(jstou8(q), jstou8(word)));
}

std::wstring DFA::getInitialState() const {
  return u8tojs(dfa::getInitialState());
}

std::vector<std::wstring> DFA::getStates() const {
  auto const& original = dfa::getStates();
  auto result = std::vector<std::wstring>();
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

std::vector<std::wstring> DFA::getAlphabet() const {
  auto const& original = dfa::getAlphabet();
  auto result = std::vector<std::wstring>();
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

bool DFA::isAccepting(std::wstring q) const {
  return dfa::isAccepting(jstou8(q));
}

std::wstring DFA::findShortestWord() const {
  return u8tojs(reg::findShortestWord(*this));
}

NFA DFA::asNFA() const {
  return static_cast<nfa>(*this);
}

bool DFA::equals(DFA other) const {
  return *this == other;
}

bool DFA::equalsNFA(NFA other) const {
  return *this == other;
}
}

EMSCRIPTEN_BINDINGS(regDFA) {
  using d = reg::DFA;

  emscripten::class_<d>("DFA")
      .constructor()
      .function("delta", &d::delta) // <std::wstring, std::wstring, std::wstring>
      .function("deltaHat", &d::deltaHat) // <std::wstring, std::wstring, std::wstring>
      .function("getInitialState", &d::getInitialState) // <std::wstring>
      .function("getStates", &d::getStates) // <std::vector<std::wstring>>
      .function("getAlphabet", &d::getAlphabet) // <std::vector<std::wstring>>
      .function("isAccepting", &d::isAccepting) // <bool, std::wstring>
      .function("findShortestWord", &d::findShortestWord) // <std::wstring>
      .function("asNFA", &d::asNFA) // <reg::NFA>
      .function("equals", &d::equals) // <bool, d>
      .function("equalsNFA", &d::equalsNFA) // <bool, reg::NFA>
    ;
}
