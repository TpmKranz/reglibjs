// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#ifndef REGJS_NFA_H
#define REGJS_NFA_H

#include "reglibcpp/nfa.h"
#include "dfa.h"

namespace reg {
struct DFA;
struct NFA : public reg::nfa {
  NFA();
  NFA(nfa n);

  std::vector<std::wstring> delta(std::wstring q, std::wstring symbol) const;
  std::vector<std::wstring> deltaHat(std::wstring q, std::wstring word) const;
  std::vector<std::wstring> deltaHatSet(std::vector<std::wstring> qs, std::wstring word) const;
  std::vector<std::wstring> epsilonClosure(std::wstring q) const;
  std::vector<std::wstring> epsilonClosureSet(std::vector<std::wstring> qs) const;
  std::wstring getInitialState() const;
  std::vector<std::wstring> getStates() const;
  std::vector<std::wstring> getAlphabet() const;
  bool isAccepting(std::wstring q) const;
  bool isAcceptingSet(std::vector<std::wstring> qs) const;
  std::wstring findShortestWord() const;
  bool equalsDFA(DFA other) const;
  bool equals(NFA other) const;
};
}

#endif
