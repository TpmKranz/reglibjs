// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#include "fabuilder.h"
#include "utils.h"

#include <emscripten/bind.h>

namespace reg {

FABuilder::FABuilder() : fabuilder() {}
FABuilder::FABuilder(NFA n) : fabuilder(n) {}
FABuilder::FABuilder(nfa n) : fabuilder(n) {}
FABuilder::FABuilder(dfa d) : fabuilder(d) {}
FABuilder::FABuilder(fabuilder b) : fabuilder(std::move(b)) {}

void FABuilder::setAccepting(std::wstring state, bool accept) {
  fabuilder::setAccepting(jstou8(state), accept);
}

void FABuilder::makeInitial(std::wstring state) {
  fabuilder::makeInitial(jstou8(state));
}

void FABuilder::addSymbol(std::wstring symbol) {
  fabuilder::addSymbol(jstou8(symbol));
}

void FABuilder::addTransition(std::wstring from, std::wstring to, std::wstring symbol) {
  fabuilder::addTransition(jstou8(from), jstou8(to), jstou8(symbol));
}

void FABuilder::powerset() {
  fabuilder::powerset();
}

void FABuilder::complete() {
  fabuilder::complement();
}

void FABuilder::merge() {
  fabuilder::merge(false);
}

void FABuilder::purge() {
  fabuilder::purge();
}

void FABuilder::minimize() {
  fabuilder::minimize(false);
}

void FABuilder::unite(NFA other) {
  fabuilder::unite(other);
}

void FABuilder::intersect(NFA other) {
  fabuilder::intersect(other);
}

void FABuilder::subtract(NFA other) {
  fabuilder::subtract(other, false);
}

void FABuilder::complement() {
  fabuilder::complement(false);
}

void FABuilder::normalizeStateNames(std::wstring prefix) {
  fabuilder::normalizeStateNames(jstou8(prefix));
}

bool FABuilder::isComplete() {
  return fabuilder::isComplete();
}

bool FABuilder::hasNondeterminism() {
  return fabuilder::hasNondeterminism();
}

NFA FABuilder::buildNFA() {
  return fabuilder::buildNfa();
}

DFA FABuilder::buildDFA() {
  return fabuilder::buildDfa(false);
}

}

EMSCRIPTEN_BINDINGS(regFABuilder) {
  using b = reg::FABuilder;
  emscripten::class_<b>("FABuilder")
      .constructor()
      .constructor<reg::NFA>()
      .function("setAccepting", &b::setAccepting) // <void, std::wstring, bool>
      .function("makeInitial", &b::makeInitial) // <void, std::wstring>
      .function("addSymbol", &b::addSymbol) // <void, std::wstring>
      .function("addTransition", &b::addTransition) // <void, std::wstring, std::wstring, std::wstring>
      .function("powerset", &b::powerset) // <void>
      .function("complete", &b::complete) // <void>
      .function("merge", &b::merge) // <void>
      .function("purge", &b::purge) // <void>
      .function("minimize", &b::minimize) // <void>
      .function("unite", &b::unite) // <void, reg::NFA>
      .function("intersect", &b::intersect) // <void, reg::NFA>
      .function("subtract", &b::subtract) // <void, reg::NFA>
      .function("complement", &b::complement) // <void>
      .function("normalizeStateNames", &b::normalizeStateNames) // <void, std::wstring>
      .function("isComplete", &b::isComplete) // <bool>
      .function("hasNondeterminism", &b::hasNondeterminism) // <bool>
      .function("buildNFA", &b::buildNFA) // <reg::NFA>
      .function("buildDFA", &b::buildDFA) // <reg::DFA>
    ;
}
