// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#include "nfa.h"
#include "utils.h"
#include <emscripten/bind.h>

namespace reg {
NFA::NFA() : nfa() {}
NFA::NFA(nfa n) : nfa(std::move(n)) {}

std::vector<std::wstring> NFA::delta(std::wstring q, std::wstring symbol) const {
  auto const& original = nfa::delta(jstou8(q), jstou8(symbol));
  auto result = std::vector<std::wstring>();
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

std::vector<std::wstring> NFA::deltaHat(std::wstring q, std::wstring word) const {
  auto const& original = nfa::deltaHat(jstou8(q), jstou8(word));
  auto result = std::vector<std::wstring>();
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

std::vector<std::wstring> NFA::deltaHatSet(std::vector<std::wstring> qs, std::wstring word) const {
  auto in = std::vector<std::string>();
  in.reserve(qs.size());
  std::transform(std::begin(qs), std::end(qs), std::back_inserter(in), jstou8);
  auto original = nfa::deltaHat(nfa::state_set(std::begin(in), std::end(in)), jstou8(word));
  auto result = std::vector<std::wstring>();
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

std::vector<std::wstring> NFA::epsilonClosure(std::wstring q) const {
  auto const& original = nfa::epsilonClosure(jstou8(q));
  auto result = std::vector<std::wstring>();
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

std::vector<std::wstring> NFA::epsilonClosureSet(std::vector<std::wstring> qs) const {
  auto in = std::vector<std::string>();
  in.reserve(qs.size());
  std::transform(std::begin(qs), std::end(qs), std::back_inserter(in), jstou8);
  auto original = nfa::epsilonClosure(nfa::state_set(std::begin(in), std::end(in)));
  auto result = std::vector<std::wstring>();
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

std::wstring NFA::getInitialState() const {
  return u8tojs(nfa::getInitialState());
}

std::vector<std::wstring> NFA::getStates() const {
  auto const& original = nfa::getStates();
  auto result = std::vector<std::wstring>();
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

std::vector<std::wstring> NFA::getAlphabet() const {
  auto const& original = nfa::getAlphabet();
  auto result = std::vector<std::wstring>();
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

bool NFA::isAccepting(std::wstring q) const {
  return nfa::isAccepting(jstou8(q));
}

bool NFA::isAcceptingSet(std::vector<std::wstring> qs) const {
  return std::any_of(qs.begin(), qs.end(), [this](std::wstring const& in)->bool{return isAccepting(in);});
}

std::wstring NFA::findShortestWord() const {
  return u8tojs(reg::findShortestWord(*this));
}

bool NFA::equalsDFA(DFA other) const {
  return *this == other;
}

bool NFA::equals(NFA other) const {
  return *this == other;
}

}

EMSCRIPTEN_BINDINGS(regNFA) {
  using n = reg::NFA;

  emscripten::class_<n>("NFA")
      .constructor()
      .function("delta", &n::delta) // <std::vector<std::wstring>, std::wstring, std::wstring>
      .function("deltaHat", &n::deltaHat) // <std::vector<std::wstring>, std::wstring, std::wstring>
      .function("deltaHatSet", &n::deltaHatSet) // <std::vector<std::wstring>, std::vector<std::wstring>, std::wstring>
      .function("epsilonClosure", &n::epsilonClosure) // <std::vector<std::wstring>, std::wstring>
      .function("epsilonClosureSet", &n::epsilonClosureSet) // <std::vector<std::wstring>, std::vector<std::wstring>>
      .function("getInitialState", &n::getInitialState) // <std::wstring>
      .function("getStates", &n::getStates) // <std::vector<std::wstring>>
      .function("getAlphabet", &n::getAlphabet) // <std::vector<std::wstring>>
      .function("isAccepting", &n::isAccepting) // <bool, std::wstring>
      .function("isAcceptingSet", &n::isAcceptingSet) // <bool, std::vector<std::wstring>>
      .function("findShortestWord", &n::findShortestWord) // <std::wstring>
      .function("equals", &n::equals) // <bool, n>
      .function("equalsDFA", &n::equalsDFA) // <bool, reg::DFA>
    ;
}
