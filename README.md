reglibjs
===
A JavaScript/WebAssembly wrapper for [reglibcpp](https://gitlab.com/TpmKranz/reglibcpp).

### Building
0. Clone this repo and initialize the [reglibcpp](https://gitlab.com/TpmKranz/reglibcpp) submodule:
   ```bash
   git clone --recurse-submodules https://gitlab.com/TpmKranz/reglibjs.git /path/to/reglibjs
   ```
1. Set up [emscripten](https://emscripten.org/docs/getting_started/downloads.html)
2. Be sure to `source` the `emsdk_env.sh` file
3. Create your build directory and go there:
   ```bash
   mkdir /path/to/build && cd /path/to/build
   ```
4. Run CMake, informing it where the JavaScript/WebAssembly files should go and what the current version is:
   ```bash
   emcmake cmake -DCMAKE_INSTALL_PREFIX=/path/to/www -DVERSION="$MAJOR.$MINOR.$PATCH" /path/to/reglibjs
   ```
5. Run Make to compile and place the files in the aforementioned `$CMAKE_INSTALL_PREFIX`:
   ```bash
   make install
   ```
