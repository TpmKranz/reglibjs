// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#include <emscripten/bind.h>
#include <vector>
#include <string>

#include "reglibcpp/utils.h"
#include "reglibcpp/fabuilder.h"
#include "utils.h"

namespace reg {
template<typename T> std::wstring extractMessage(size_t eptr) {
  T* ptr = static_cast<T*>(*static_cast<void**>(static_cast<void*>(&eptr)));
  std::wstring msg = u8tojs(ptr->what());
  delete ptr;
  return msg;
}

}

EMSCRIPTEN_BINDINGS(regSTL) {
  emscripten::register_vector<std::wstring>("JSStringVector");
  emscripten::register_vector<std::pair<std::wstring, std::wstring>>("JSStringPairVector");
  emscripten::class_<std::pair<std::wstring, std::wstring>>("JSStringPair")
      .constructor<std::wstring, std::wstring>()
      .property("first", &std::pair<std::wstring, std::wstring>::first)
      .property("second", &std::pair<std::wstring, std::wstring>::second)
    ;
  emscripten::function<std::wstring, size_t>("extractMessageStateNotFound", &reg::extractMessage<reg::state_not_found>);
  emscripten::function<std::wstring, size_t>("extractMessageSymbolNotFound", &reg::extractMessage<reg::symbol_not_found>);
  emscripten::function<std::wstring, size_t>("extractMessageNondeterminismException", &reg::extractMessage<reg::fabuilder::nondeterminism_exception>);
  emscripten::function<std::wstring, size_t>("extractMessageInvalidArgument", &reg::extractMessage<std::invalid_argument>);
}
