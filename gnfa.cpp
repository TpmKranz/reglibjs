// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#include "gnfa.h"
#include "utils.h"
#include <emscripten/bind.h>

namespace reg {
GNFA::GNFA(gnfa g) : gnfa(std::move(g)) {}
GNFA::GNFA(dfa d) : gnfa(d) {}
GNFA::GNFA(nfa n) : gnfa(n) {}
GNFA::GNFA(expression::exptr e) : gnfa(e) {}

std::wstring GNFA::getInitialState() const {
  return u8tojs(gnfa::getInitialState());
}

std::wstring GNFA::getAcceptingState() const {
  return u8tojs(gnfa::getAcceptingState());
}

std::vector<std::wstring> GNFA::getActiveStates() const {
  auto original = gnfa::getActiveStates();
  std::vector<std::wstring> result;
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

Expression GNFA::getTransition(std::wstring q, std::wstring p) const {
  return gnfa::getTransition(jstou8(q), jstou8(p));
}

std::vector<std::pair<std::wstring, std::wstring>> GNFA::getSplittableTransitions() const {
  auto original = gnfa::getSplittableTransitions();
  std::vector<std::pair<std::wstring, std::wstring>> result;
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result),
                 [](state_pair_vector::value_type const& in){
                    return std::make_pair(u8tojs(in.first), u8tojs(in.second));
                  });
  return result;
}

std::vector<std::wstring> GNFA::splitTransition(std::wstring q, std::wstring p) {
  auto original = gnfa::splitTransition(jstou8(q), jstou8(p));
  std::vector<std::wstring> result;
  result.reserve(original.size());
  std::transform(original.begin(), original.end(), std::back_inserter(result), u8tojs);
  return result;
}

NFA GNFA::splitAllTransitions() {
  return gnfa::splitAllTransitions();
}

void GNFA::bypassTransition(std::wstring q, std::wstring p) {
  gnfa::bypassTransition(jstou8(q), jstou8(p));
}

void GNFA::bypassTransitionOffer(std::wstring q, std::wstring p, std::map<std::wstring, Expression> offer) {
  std::unordered_map<std::string, expression::exptr> inoffer(offer.size());
  for (auto const& entry : offer) {
    inoffer.emplace(jstou8(entry.first), entry.second.wrapee);
  }
  gnfa::bypassTransition(jstou8(q), jstou8(p), inoffer);
}

void GNFA::ripStateOffer(std::wstring q, std::map<std::wstring, std::map<std::wstring, Expression>> offer) {
  std::unordered_map<std::string, std::unordered_map<std::string, expression::exptr>> inoffer(offer.size());
  for (auto const& outerentry : offer) {
    std::unordered_map<std::string, expression::exptr> inner(outerentry.second.size());
    for (auto const& entry : outerentry.second) {
      inner.emplace(jstou8(entry.first), entry.second.wrapee);
    }
    inoffer.emplace(jstou8(outerentry.first), std::move(inner));
  }
  gnfa::ripState(jstou8(q), inoffer);
}

void GNFA::ripState(std::wstring q) {
  gnfa::ripState(jstou8(q));
}

Expression GNFA::ripAllStates() {
  return gnfa::ripAllStates();
}

}

EMSCRIPTEN_BINDINGS(regGNFA) {
  using g = reg::GNFA;

  emscripten::class_<g>("GNFA")
      .function("getInitialState", &g::getInitialState) // <std::wstring>
      .function("getAcceptingState", &g::getAcceptingState) // <std::wstring>
      .function("getActiveStates", &g::getActiveStates) // <std::vector<std::wstring>>
      .function("getTransition", &g::getTransition) // <reg::Expression, std::wstring, std::wstring>
      .function("getSplittableTransitions", &g::getSplittableTransitions) // <std::vector<std::pair<std::wstring, std::wstring>>>
      .function("splitTransition", &g::splitTransition) // <std::vector<std::wstring>, std::wstring, std::wstring>
      .function("splitAllTransitions", &g::splitAllTransitions) // <reg::NFA>
      .function("bypassTransition", &g::bypassTransition) // <void, std::wstring, std::wstring>
      .function("bypassTransition", &g::bypassTransitionOffer) // <void, std::wstring, std::wstring, std::map<std::wstring, reg::Expression>>
      .function("ripState", &g::ripState) // <void, std::wstring>
      .function("ripStateOffer", &g::ripStateOffer) // <void, std::wstring, std::map<std::wstring, std::map<std::wstring, reg::Expression>>>
      .function("ripAllStates", &g::ripAllStates) // <reg::Expression>
    ;

  //emscripten::register_map<std::wstring, reg::Expression>("JSStringExpressionMap");
  // FIXME: Workaround until https://github.com/emscripten-core/emscripten/pull/9348 goes through
  {
    using namespace emscripten;
    const char* name = "JSStringExpressionMap";
    using K = std::wstring;
    using V = reg::Expression;

    using MapType = std::map<K,V>;

    size_t (MapType::*size)() const = &MapType::size;
    class_<MapType>(name)
          .template constructor<>()
          .function("size", size)
          .function("get", internal::MapAccess<MapType>::get)
          .function("set", internal::MapAccess<MapType>::set)
          .function("keys", internal::MapAccess<MapType>::keys)
          ;
  }
  //emscripten::register_map<std::wstring, std::map<std::wstring, reg::Expression>>("JSStringJSStringExpressionMapMap");
  // FIXME: Workaround until https://github.com/emscripten-core/emscripten/pull/9348 goes through
  {
    using namespace emscripten;
    const char* name = "JSStringJSStringExpressionMapMap";
    using K = std::wstring;
    using V = std::map<std::wstring, reg::Expression>;

    using MapType = std::map<K,V>;

    size_t (MapType::*size)() const = &MapType::size;
    class_<MapType>(name)
          .template constructor<>()
          .function("size", size)
          .function("get", internal::MapAccess<MapType>::get)
          .function("set", internal::MapAccess<MapType>::set)
          .function("keys", internal::MapAccess<MapType>::keys)
          ;
  }
  emscripten::function<g, reg::DFA>("GNFAFromDFA", &reg::GNFAFromDFA);
  emscripten::function<g, reg::NFA>("GNFAFromNFA", &reg::GNFAFromNFA);
  emscripten::function<g, reg::Expression>("GNFAFromExpression", &reg::GNFAFromExpression);
}
