// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#ifndef REGJS_GNFA_H
#define REGJS_GNFA_H

#include "reglibcpp/gnfa.h"
#include "expression.h"
#include "nfa.h"
#include "dfa.h"

#include <map>

namespace reg {
struct GNFA : public gnfa {
  GNFA(gnfa g);
  GNFA(dfa d);
  GNFA(nfa n);
  GNFA(expression::exptr e);

  std::wstring getInitialState() const;
  std::wstring getAcceptingState() const;
  std::vector<std::wstring> getActiveStates() const;
  Expression getTransition(std::wstring q, std::wstring p) const;
  std::vector<std::pair<std::wstring, std::wstring>> getSplittableTransitions() const;
  std::vector<std::wstring> splitTransition(std::wstring q, std::wstring p);
  NFA splitAllTransitions();
  void bypassTransition(std::wstring q, std::wstring p);
  void bypassTransitionOffer(std::wstring q, std::wstring p, std::map<std::wstring, Expression> offer);
  void ripState(std::wstring q);
  void ripStateOffer(std::wstring q, std::map<std::wstring, std::map<std::wstring, Expression>> offer);
  Expression ripAllStates();
};

inline GNFA GNFAFromDFA(DFA d) { return GNFA(d); }
inline GNFA GNFAFromNFA(NFA n) { return GNFA(n); }
inline GNFA GNFAFromExpression(Expression e) { return GNFA(e.wrapee); }
}

#endif
