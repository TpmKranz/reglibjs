// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#ifndef REGJS_DFA_H
#define REGJS_DFA_H

#include "reglibcpp/dfa.h"
#include "nfa.h"

namespace reg {
struct NFA;
struct DFA : public dfa {
  DFA();
  DFA(dfa d);
  std::wstring delta(std::wstring q, std::wstring symbol) const;
  std::wstring deltaHat(std::wstring q, std::wstring word) const;
  std::wstring getInitialState() const;
  std::vector<std::wstring> getStates() const;
  std::vector<std::wstring> getAlphabet() const;
  bool isAccepting(std::wstring q) const;
  std::wstring findShortestWord() const;
  NFA asNFA() const;
  bool equals(DFA other) const;
  bool equalsNFA(NFA other) const;
};
}

#endif
