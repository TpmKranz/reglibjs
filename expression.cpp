// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#include "expression.h"
#include "reglibcpp/nfa.h"
#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(regExpression) {
  using e = reg::Expression;

  emscripten::class_<e>("Expression")
      .function("toString", &e::toString) // <std::wstring>
      .function("extractSymbol", &e::extractSymbol) // <std::wstring>
      .function("getOperation", &e::getOperation) // <reg::expression::operation>
      .function("equals", &e::equals) // <bool, e>
      .function("equalsNFA", &e::equalsNFA) // <bool, reg::NFA>
      .function("asNFA", &e::asNFA) // <reg::NFA>
      .function("size", &e::size) // <size_t>
    ;

  emscripten::function<e>("REEmptySet", &reg::REEmptySet);
  emscripten::function<e>("REEmptyString", &reg::REEmptyString);
  emscripten::function<e, std::wstring>("RESymbol", &reg::RESymbol);
  emscripten::function<e, e, e>("REAlternation", &reg::REAlternation);
  emscripten::function<e, e, e>("REAlternationO0", &reg::REAlternationO0);
  emscripten::function<e, e, e>("REAlternationO1", &reg::REAlternationO1);
  emscripten::function<e, e, e>("REAlternationO2", &reg::REAlternationO2);
  emscripten::function<e, e, e>("REConcatenation", &reg::REConcatenation);
  emscripten::function<e, e, e>("REConcatenationO0", &reg::REConcatenationO0);
  emscripten::function<e, e, e>("REConcatenationO1", &reg::REConcatenationO1);
  emscripten::function<e, e, e>("REConcatenationO2", &reg::REConcatenationO2);
  emscripten::function<e, e>("REKleene", &reg::REKleene);
  emscripten::function<e, e>("REKleeneO0", &reg::REKleeneO0);
  emscripten::function<e, e>("REKleeneO1", &reg::REKleeneO1);
  emscripten::function<e, e>("REKleeneO2", &reg::REKleeneO2);
  emscripten::function<e, std::wstring>("REFromString", &reg::REFromString);
  emscripten::function<e, std::wstring>("REFromStringO0", &reg::REFromStringO0);
  emscripten::function<e, std::wstring>("REFromStringO1", &reg::REFromStringO1);
  emscripten::function<e, std::wstring>("REFromStringO2", &reg::REFromStringO2);

  emscripten::function<void, std::wstring>("RESetLeftParenthesis", &reg::RESetLeftParenthesis);
  emscripten::function<void, std::wstring>("RESetRightParenthesis", &reg::RESetRightParenthesis);
  emscripten::function<void, std::wstring>("RESetKleeneStar", &reg::RESetKleeneStar);
  emscripten::function<void, std::wstring>("RESetAlternationSign", &reg::RESetAlternationSign);
  emscripten::function<void, std::wstring>("RESetEmptyString", &reg::RESetEmptyString);
  emscripten::function<void, std::wstring>("RESetEmptySet", &reg::RESetEmptySet);
  emscripten::function<std::wstring>("REGetLeftParenthesis", &reg::REGetLeftParenthesis);
  emscripten::function<std::wstring>("REGetRightParenthesis", &reg::REGetRightParenthesis);
  emscripten::function<std::wstring>("REGetKleeneStar", &reg::REGetKleeneStar);
  emscripten::function<std::wstring>("REGetAlternationSign", &reg::REGetAlternationSign);
  emscripten::function<std::wstring>("REGetEmptyString", &reg::REGetEmptyString);
  emscripten::function<std::wstring>("REGetEmptySet", &reg::REGetEmptySet);

  emscripten::register_vector<e>("eVector");

  emscripten::enum_<reg::expression::operation>("REOperation")
      .value("empty", reg::expression::operation::empty)
      .value("symbol", reg::expression::operation::symbol)
      .value("kleene", reg::expression::operation::kleene)
      .value("concatenation", reg::expression::operation::concatenation)
      .value("alternation", reg::expression::operation::alternation)
    ;
}
