// Copyright 2018 Tom Kranz
//
// This file is part of RegApp.
//
// RegApp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RegApp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

#ifndef REGJS_UTILS_H
#define REGJS_UTILS_H

#include <codecvt>
#include <locale>

namespace reg {
inline std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t>
    jsconverter;  ///< Converts between UTF-8-encoded and “JS wide” (UTF-16-LE) strings.
inline auto jstou8 = [](std::wstring const& in){ return jsconverter.to_bytes(in); };
inline auto u8tojs = [](std::string const& in){ return jsconverter.from_bytes(in); };
}

#endif
